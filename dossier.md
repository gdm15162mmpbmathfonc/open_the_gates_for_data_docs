# Werkstuk 2e zit - Open the gates for data

	2MMP proDUCE a (2MMPB)
	Mathias Fonck & Thomas Van Der Sypt
	NMDAD I
	Academiejaar 2015 - 2016
	Grafische en digitale media
	Multimediaproductie
	Artevelde Hogeschool

## Briefing & Analyse
Wat te doen in Gent? Onze app 9000 geeft je een bondig overzicht van wat er al te doen is. Kunstgallerijen, bibliotheken, bioscopen, sportcentra, maar ook parkings en stations worden aan de hand van datasets op een kaart getoont. De gebruiker krijgt een overzicht van wat er waar te doen is in Gent. Indien de dataset het toelaat toont onze app ook informatie over het ‘wat en waar’ in Gent. De gebruiker hoeft enkel te klikken op een marker op de kaart en de informatie wordt hem getoont.

Daarnaast heeft onze app ook een quiz voor degene die zich verveelt. In deze quiz ondervraagt onze app de gebruiker om te achterhalen wat de gebruiker het best zou kunnen doen. Vervolgens toont onze app waar hij deze activiteiten kan doen en geeft eventueel (afhankelijk van de dataset) informatie hieromtrent.

## Specificaties

### Functionele Specificaties
* Contactformulier
* Maandelijkse nieuwsbrief
* Quiz

### Niet-functionele Specificaties
* Responsive, mobile-first
* Webapp met verschillende pagina's 
* Inhoud wordt grotendeels in lokale databestanden beheerd
* Google Maps integratie
* Tonen van geolocatie gebruiker
* Interactie met gebruiker

### Technische Specificaties
* HTML5
* CSS
* JavaScript
* jQuery
* JSON

## Ideeënborden
![Ideeënbord](images/ideeënbord.png)

## Wireframe E-mailsjabloon
![Wireframe e-mailsjabloon](images/e-mailsjabloon.png)

## Wireflow Webapp
![Wireflow](images/wireflow.png)

## Moodboard
![Moodboard 1](images/moodboard.png)
![Moodboard 2](images/moodboard2.png)

## Style Tiles
![Style tile (1)](images/style-tile1.png)
![Style tile (2)](images/style-tile2.png)
![Style tile (3)](images/style-tile3.png)

![Logos](images/logos.png)

## Visual Design
![Visual Design](images/visual.png)

## Screenshots
### Screenshots Webapp
![Desktop screenshot (1)](images/desktop1.png)
![Desktop screenshot (2)](images/desktop2.png)
![Desktop screenshot (3)](images/desktop3.png)
![Mobile screenshot (1)](images/mobile1.png)
![Mobile screenshot (2)](images/mobile2.png)
![Mobile screenshot (3)](images/mobile3.png)

### Screenshots Codesnippets
![Code snippet JS (1)](images/codesnippet1.png)
![Code snippet JS (2)](images/codesnippet2.png)
![Code snippet JS (3)](images/codesnippet3.png)
![Code snippet HTML](images/codesnippet4.png)
![Code snippet CSS](images/codesnippet5.png)


