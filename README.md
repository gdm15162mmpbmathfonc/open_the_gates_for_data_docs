# open_the_gates_for_data_docs: Read Me
    authors: Mathias Fonck & Thomas Van Der Sypt

Wat te doen in Gent? Onze app 9000 geeft je een bondig overzicht van wat er al te doen is. Kunstgallerijen, bibliotheken, bioscopen, sportcentra, maar ook parkings en stations worden aan de hand van datasets op een kaart getoont. De gebruiker krijgt een overzicht van wat er waar te doen is in Gent. Indien de dataset het toelaat toont onze app ook informatie over het ‘wat en waar’ in Gent. De gebruiker hoeft enkel te klikken op een marker op de kaart en de informatie wordt hem getoont.

Daarnaast heeft onze app ook een quiz voor degene die zich verveelt. In deze quiz ondervraagt onze app de gebruiker om te achterhalen wat de gebruiker het best zou kunnen doen. Vervolgens toont onze app waar hij deze activiteiten kan doen en geeft eventueel (afhankelijk van de dataset) informatie hieromtrent.

[dossier (pdf)](dossier.pdf)

[dossier (Markdown)](dossier.md)

[poster](poster.pdf)

[timesheet](timesheet.xlsx)
